// declaracion de variables
const inputs = document.querySelectorAll("datos-de-libro"),
    adver = document.getElementById("warning");
// declaracion de variable para guardar el valor de la contraseÃ±a
var pass;
var letras ="abcdefghijklmnopqrstuvwxyz";
var numeros = "0123456789";

// declaracion de las expresiones regulares para la validacion
const expresionesRegulares = {
    codigo: /([0-5])/,
    titulo: /([0-100])/,
    autor: /([0-60])/,
    autor: /([0-30])/,
    text: /([a-zA-Z])/,
    name: /([a-zA-Z])/,
    caracteres: /[^a-zA-Z\d\s]/,
    espacios: /\s/g,
};

function validar_letras(texto){
    texto = texto.toLowerCase();
    for(i=0; i<texto.length; i++){
        if (letras.indexOf(texto.charAt(i),0)!=-1){
            return 1;
        }
    }
    return 0;
};

//Esta función se puede probar con estas sentencias:

alert(tiene_letras("1"));
alert(tiene_letras("22323232s"));
alert(tiene_letras("22323232sf"));
alert(tiene_letras("a2232323"));
alert(tiene_letras("A22323G2.12"));

function validar_numeros(texto){
    for(i=0; i<texto.length; i++){
        if (numeros.indexOf(texto.charAt(i),0)!=-1){
            return 1;
        }
    }
    return 0;
};

//Esta función se puede probar con estas sentencias:

alert(tiene_numeros("ASAS1"));
alert(tiene_numeros("2asasasas"));
alert(tiene_numeros("asas2sG"));
alert(tiene_numeros("a...."));
alert(tiene_numeros("A22323G2.12"));

